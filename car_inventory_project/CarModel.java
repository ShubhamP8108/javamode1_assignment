package com.car_inventory_project;
import lombok.*;
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CarModel {
	private String car_Id;
	private String car_make;
	private String car_model;
	private int car_year;
	private double car_sales_price;
	@Override
	public String toString() {
		String str=String.format("%-12s %-16s %-16s %-20s $%-7s", car_Id,car_year,car_make,car_model,car_sales_price);
		return str;
	}
}
