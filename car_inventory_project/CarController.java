package com.car_inventory_project;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class CarController {

	public static void main(String[] args) {
		System.out.println(String.format("%-60s","****Welcome to the Shubham Pandey Auto Dealers****"));
		Scanner sc=new Scanner(System.in);
		String answer;
		CarViewMethods carViewMeth=new CarViewMethods();
		do {
		System.out.println("1. List\n2. Add\n3. Delete\n4. Update\n5. Quit\nEnter the option only in lowercase");
		System.out.print("Enter command: ");
		answer=sc.nextLine();
		switch(answer) {
		case "list":
			List<CarModel> carList=carViewMeth.displayCar();
			int num_of_Cars=carViewMeth.carCount();
			System.out.println("Number of cars: "+num_of_Cars);
			String str=String.format("%-12s %-16s %-16s %-17s %-7s", "Car ID","Making Year","Make_By","Model Name","Sales Price(in $)");
			System.out.println(str+"\n");
			for (CarModel car:carList) {
				System.out.println(car);
			}
			double total=carList.stream().map(car1->car1.getCar_sales_price()).collect(Collectors.summingDouble(Double::doubleValue));
			System.out.println("\nThe total cost of the cars :    $"+total+"\n");
			break;
		case "add":
			try {
			int num_Of_Cars=carViewMeth.carCount();
			if (num_Of_Cars<20) {
			System.out.print("Car ID: ");
			String carID=sc.nextLine();
			System.out.println();
			System.out.print("Make: ");
			String make=sc.nextLine();
			System.out.println();
			System.out.print("Model: ");
			String model=sc.nextLine();
			System.out.println();
			System.out.print("Year: ");
			int year=sc.nextInt();
			System.out.println();
			System.out.print("Price: ");
			double price=sc.nextDouble();
			boolean flag=carViewMeth.carAdd(carID, make, model, year, price);
			if (flag==true) {
				System.out.println("Added successfully");}
			else {
				System.out.println("Could not add it");}
			}
			else {
				System.out.println("maximum capacity reached out. Delete some entries to add more.......");
				break;
			}
			sc.nextLine();
			}catch(InputMismatchException ime) {
				System.out.println("Enter proper input");
				sc.nextLine();
			}
		break;
		case "update":
			System.out.println("Enter the ID of the car you want to update");
			String carId=sc.nextLine();
			System.out.println("Enter the field you want to update");
			System.out.println(" 1.Make\n 2.Model\n 3.Year\n 4.Price\nEnter the option only in lowercase");
			String fieldOption=sc.nextLine();
			switch(fieldOption) {
			case "make":
				System.out.println("Enter the new make");
				String make=sc.nextLine();
				boolean flagMake=carViewMeth.carUpdateMake(carId, make);
				if (flagMake==true)
					System.out.println("Updated successfully");
				else
					System.out.println("Could not update");
			break;
			case "model":
				System.out.println("Enter the new model name");
				String model=sc.nextLine();
				boolean flagModel=carViewMeth.carUpdateModel(carId, model);
				if (flagModel==true)
					System.out.println("Updated successfully");
				else
					System.out.println("Could not update");
			break;
			case "year":
				try {
				System.out.println("Enter the new year");
				int year=Integer.parseInt(sc.nextLine());
				boolean flagYear=carViewMeth.carUpdateYear(carId, year);
				if (flagYear==true)
					System.out.println("Updated successfully");
				else
					System.out.println("Could not update");
				}catch(InputMismatchException ime) {
					System.out.println("Enter integer input");
				}catch(NumberFormatException nfe) {
					System.out.println("Enter proper input");
				}
			break;
			case "price":
				try {
				System.out.println("Enter the new price");
				double price=Double.parseDouble(sc.nextLine());
				boolean flagPrice=carViewMeth.carUpdatePrice(carId, price);
				if (flagPrice==true)
					System.out.println("Updated successfully");
				else
					System.out.println("Could not update");}
				catch(InputMismatchException ime) {
					System.out.println("Enter double input");
				}catch(NumberFormatException nfe) {
					System.out.println("Enter proper input");
				}
			break;
			default:
				System.out.println("Enter a valid field");
				
			}
		break;
		case "delete":
			System.out.println("Enter the ID of the car you want to delete");
			String carID=sc.nextLine();
			boolean status=carViewMeth.carDelete(carID);
			if (status==true)
				System.out.println("Deleted successfully");
			else
				System.out.println("Could not delete");
		break;
		case "quit":
			System.out.println("Console Program Ended.");
		break;
		
		default:
			System.out.println("Sorry but \""+answer+"\" is not a valid command. Please try again.");
		break;
		}	
		}while(!answer.equalsIgnoreCase("quit"));
		
		sc.close();
	}

	}


