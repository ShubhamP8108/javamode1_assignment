package com.listenerdemo;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class MyFirstListener implements HttpSessionListener {  
    public MyFirstListener() {
    }
    public void sessionCreated(HttpSessionEvent se)  { 
     System.out.println("Session Created");
    }
    public void sessionDestroyed(HttpSessionEvent se)  { 
    	 System.out.println("Session Destroyed");
    }
	
}
