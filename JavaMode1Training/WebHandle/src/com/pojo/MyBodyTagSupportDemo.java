package com.pojo;


import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class MyBodyTagSupportDemo extends BodyTagSupport{
	private String color;
	public void setColor(String color) {
		this.color = color;
	}
	public int doAfterBody() {
		try {
		BodyContent bc=getBodyContent();
		String bodyContent=bc.getString();
		JspWriter out=bc.getEnclosingWriter();
		out.println(bodyContent);
		out.println("<body bgcolor="+color);
		}
		catch(Exception e) {
			
		}
		
		return SKIP_BODY;
	}

}
