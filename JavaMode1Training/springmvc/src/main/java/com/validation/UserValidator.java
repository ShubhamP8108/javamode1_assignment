package com.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.model.User;
@Component(value="userValidator")
public class UserValidator implements Validator{

	@Override
	public boolean supports(Class aClass) {
		return aClass.equals(User.class);
	}

	@Override
	public void validate(Object obj, Errors error) {
		ValidationUtils.rejectIfEmpty(error,"userName","errorCode.userName");
		ValidationUtils.rejectIfEmpty(error,"userPassword", "errorCode.userPassword");
		
		
	}

}
