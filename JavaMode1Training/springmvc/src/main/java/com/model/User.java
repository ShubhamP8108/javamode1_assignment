package com.model;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
//Model ==bean validation=>validation fails ???
public class User {
	private int userId;
	private String userName;
	private String userPassword;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	
	

}
