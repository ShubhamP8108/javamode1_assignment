package com.controller;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.model.User;
import com.validation.UserValidator;

//wire with spring
@Controller
public class AppController {
	//html <form action="??" method="get" default
	//request handler
/*	@RequestMapping(value="one",method=RequestMethod.GET)
	public String functionOne(){
		return "success";
	}*/
	/*@GetMapping(value="/")
	public ModelAndView loginForm() {
		//create an object and sent to jsp page
		User user=new User();
		ModelAndView modelAndView=new ModelAndView ("login");     //bag
	    modelAndView.addObject("userPojo",user);                  //box
		
		return modelAndView;
		
	}*/
	@Autowired
	@Qualifier("userValidator")
	private UserValidator validator;
	    @InitBinder
	    private void initBinder(WebDataBinder binder) {
	        binder.setValidator(validator);
	    }
	
	@GetMapping(value="/")
	public String loginForm(Locale locale, Model model) {
		//create an object and sent to jsp page
		User user=new User();
	    model.addAttribute("userPojo",user);                 
		return "login";
		
	}
	//@RequestMapping(value="loginProcess",method=RequestMethod.POST)
	@PostMapping(value="LoginProcess")
	public ModelAndView functionTwo(@Validated @ModelAttribute("userPojo") User user,BindingResult bindingResult) { //accept ui data
		ModelAndView modelAndView=null;
		if (bindingResult.hasErrors()) {
			modelAndView=new ModelAndView("login");
			//go the page from where you came and display why the validation failed
			//modelAndView.setViewName("login");
		}else {
			modelAndView=new ModelAndView("loginsuccess");
			modelAndView.addObject("userPojo",user);
		}
		return modelAndView; //view name ---DS --->ViewResolver
	}

}
