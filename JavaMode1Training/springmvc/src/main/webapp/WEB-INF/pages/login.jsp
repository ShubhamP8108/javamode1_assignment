<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
.error {
	color: red
}
</style>
</head>
<body>
	<form:form action="LoginProcess" method="post" modelAttribute="userPojo">
<spring:message code="lbl.userId"/>:<form:input path="userId" />
<spring:message code="lbl.userName"/>:<form:input path="userName" />
		<form:errors path="userName" cssClass="error"></form:errors>
<spring:message code="lbl.userPassword"/>:<form:password path="userPassword" />
		<!-- path match property name of the pojo -->
		<form:errors path="userPassword" cssClass="error"></form:errors>
<input type="submit" value="<spring:message code="btn.submit"/>">
	</form:form>
</body>
</html>