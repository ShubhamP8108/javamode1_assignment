package com.exception;
//user defined Exception
public class UserException extends Exception {
	private String errMessage;

	public UserException(String errMessage) {
		super();
		this.errMessage = errMessage;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return this.errMessage;
	}

}
