package com.exception;

import java.io.IOException;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalException {
	@ExceptionHandler({IOException.class,UserException.class})
	public ModelAndView processExeception(Exception exception) {
		 System.out.println(exception.getClass().getSimpleName());
	     ModelAndView modelAndView = new ModelAndView("exceptionPage");	 //404
	     modelAndView.addObject("reason","Reason For Exception"+exception.getMessage()); //jsp exceptionpage
	     return modelAndView;
	}

}
