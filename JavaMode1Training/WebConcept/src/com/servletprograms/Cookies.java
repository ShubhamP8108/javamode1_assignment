package com.servletprograms;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Cookies")
public class Cookies extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html");
	        PrintWriter out = response.getWriter();
	        String name = request.getParameter("username");
	        out.println("Welcome\t"+name);
	        String encodecookie=URLEncoder.encode(name,"UTF-8");
	        Cookie cookie = new Cookie("name", encodecookie);
	        response.addCookie(cookie);
	        out.println("<form action='Cookies1'>");
	        out.println("<input type='submit' value='submit'>");
	}

}
