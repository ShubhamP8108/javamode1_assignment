package com.day2;

class Box3{
	double width;
	double depth;
	double height;
	double volume() {
		return width*depth*height;
	}
/*	public Box3() {
		width=10;
		depth=10;
		height=10;
	}*/ //this is default constructor and we need to initialize values for different objects
	//That's why we created parameterized constructor which replaced default constructor.
	public Box3(double width, double depth, double height) {
		super();
		this.width = width;
		this.depth = depth;
		this.height = height;
	}
	
	/*public Box3(double w, double d, double h) {
		width = w;
	    depth = d;
		height = h;
	}*/
	
	
}
public class Main4 {

	public static void main(String[] args) {
		Box3 b1=new Box3(10,15,20);
		Box3 b2=new Box3(2,4,6);
		
		double vol;
		vol=b1.volume();
		System.out.println("Volume is "+vol);
		vol=b2.volume();
		System.out.println("Volume is "+vol);

	}

}
