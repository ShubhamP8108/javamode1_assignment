package com.day2;

class Box2{
	double width;
	double depth;
	double height;
	
	double volume() {
		return width*depth*height;
	}
	void setDim(double w,double d,double h) {
		width=w;
		depth=d;
		height=h;
	}
}
public class Main3 {

	public static void main(String[] args) {
	Box2 b1=new Box2();
	Box2 b2=new Box2();
	
	b1.setDim(10, 15, 20);
	b2.setDim(3, 6, 9);
	
	double vol;
	vol=b1.volume();
	System.out.println("Volume is "+vol);
	vol=b2.volume();
	System.out.println("Volume is "+vol);

	}

}
