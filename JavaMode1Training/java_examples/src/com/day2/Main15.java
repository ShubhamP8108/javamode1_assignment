package com.day2;

class Overload{
	void test(int...b) {
		for(int b1:b)
			System.out.println(b1);
	}
	void test(boolean...b) {
		for(boolean b2:b)
			System.out.println(b2);
	}
	void test(int i,String...s) {
		System.out.println(i);
		for(String s1:s)
			System.out.println(s1);
	}
}
public class Main15 {

	public static void main(String...args) {
		Overload ob=new Overload();
		ob.test(1,2,3,4,4);
		ob.test(true,false,true);
		ob.test(10,"hello","world","welcome");

	}

}
