package com.day2;

public class Main8 {
 static int a=4;
 static int b;
 static {
	 System.out.println("Static block initialized");
	 b=a*3;
 }
 static void meth(int x) {
	 System.out.println(x);
	 System.out.println(a);
	 System.out.println(b);
 }
	public static void main(String[] args) {
		meth(42);

	}

}
