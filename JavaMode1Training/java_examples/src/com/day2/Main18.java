package com.day2;
class Box6{
	private double width;
	private double depth;
	private double height;
	
	double volume() {
		return width*depth*height;
	}
Box6(){
	width=-1;
	depth=-1;
	height=-1;
}
Box6(double w,double d,double h){
	width=w;
	depth=d;
	height=h;
}
Box6(double len){
	width=depth=height=len;
}
Box6(Box6 b){
	width=b.width;
	depth=b.depth;
	height=b.height;
}
}
class BoxWeight1 extends Box6{
	double weight;
	
	BoxWeight1(double w,double d,double h,double m){
		super(w,d,h);
		weight=m;
	}
	BoxWeight1(){
		super();
		weight=-1;
	}
	BoxWeight1(double len,double m){
		super(len);
		weight=m;
	}
	BoxWeight1(BoxWeight1 b){
		super(b);
		weight=b.weight;
	}
	
}
public class Main18 {

	public static void main(String[] args) {
		BoxWeight1 b1=new BoxWeight1(10,20,30,40);
		BoxWeight1 b2=new BoxWeight1();
		BoxWeight1 b3=new BoxWeight1(2,3);
		BoxWeight1 b4=new BoxWeight1(b1);
		double vol;
		vol=b1.volume();
		System.out.println("Volume is "+vol);
		System.out.println("Weight is "+b1.weight);
		vol=b2.volume();
		System.out.println("\nVolume is "+vol);
		System.out.println("Weight is "+b2.weight);
		vol=b3.volume();
		System.out.println("\nVolume is "+vol);
		System.out.println("Weight is "+b3.weight);
		vol=b4.volume();
		System.out.println("\nVolume is "+vol);
		System.out.println("Weight is "+b4.weight);
	}

}
