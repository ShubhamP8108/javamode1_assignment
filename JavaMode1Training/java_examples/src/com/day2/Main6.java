package com.day2;
class Box4{
	double width;
	double depth;
	double height;
	double volume() {
		return width*depth*height;
	}
	
 Box4() {
		width=-1;
		depth=-1;
		height=-1;
	}

 Box4(double w, double d, double h) {
	width = w;
	depth = d;
	height = h;
}
Box4(Box4 b){
	width=b.width;
	depth=b.depth;
	height=b.height;
}

 Box4(double len) {
	width = depth=height=len;
}

}

public class Main6 {
	
	public static void main(String[] args) {
		Box4 b1=new Box4();
		Box4 b2=new Box4(10,15,20);
		Box4 b3=new Box4(7);
		Box4 b4=new Box4(b2);
		double vol;
		vol=b1.volume();
		System.out.println("Volume is "+vol);
		vol=b2.volume();
		System.out.println("Volume is "+vol);
		vol=b3.volume();
		System.out.println("Volume is "+vol);
		vol=b4.volume();
		System.out.println("Volume is "+vol);

	}

}
