package com.day2;

class Box5{
	double width;
	double depth;
	double height;
	
	double volume() {
		return width*depth*height;
	}
	Box5(){
		width=-1;
		depth=-1;
		height=-1;
	}
	Box5(double w,double d,double h){
		width=w;
		depth=d;
		height=h;
	}
	Box5(double len){
		width=depth=height=len;
	}
	Box5(Box5 b){
		width=b.width;
		depth=b.depth;
		height=b.height;
	}
}

class BoxWeight extends Box5{
	double weight;
	BoxWeight(double w,double d,double h,double m){
		width=w;
		depth=d;
		height=h;
		weight=m;
	}
}
public class Main17 {

	public static void main(String[] args) {
		BoxWeight b1=new BoxWeight(10,15,20,30);
		BoxWeight b2=new BoxWeight(1,2,3,4);
		double vol;
		vol=b1.volume();
		System.out.println("Volume is "+vol);
		System.out.println("Weight is "+b2.weight);

	}

}
