package com.day2;

class A{
	A(){
		this(10);
		System.out.println("1");
	}
	A(int a){
		this("hello");
		System.out.println("2");
	}
	A(String s){
		//this();
		System.out.println("3");
	}
}
public class Main7 {

	public static void main(String[] args) {
		A a1=new A();
		//A a2=new A(10);
	//	A a3=new A("hello"); //213

	}

}
