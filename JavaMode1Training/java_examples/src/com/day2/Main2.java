package com.day2;

class Box1{
	double width;
	double depth;
	double height;
//	void volume() {
//		System.out.println(width*depth*height);
//	} 
	//Note:-This does not have return type and we have to write the output in main only
	double volume() {
		return width*depth*height;
	}
}
public class Main2 {

	public static void main(String[] args) {
		Box1 b1=new Box1();
		Box1 b2=new Box1();
		b1.width=10;b1.depth=15;b1.height=20;
		b2.width=3;b2.depth=6;b2.height=9;
		
		double vol;
		vol=b1.volume();
		System.out.println("Volume is "+vol);
		
		vol=b2.volume();
		System.out.println("Volume is "+vol);
		
		

	}

}
