package com.pack;
public class MethodReference {
	public static void m1() {
		for(int i=0;i<10;i++) {
			System.out.println("Child Thread");
		}
	}
	public static void main(String[] args) {
		Runnable r=()->{
			for(int i=0;i<10;i++) {
				System.out.println("Child Thread");
			}
		};
		Thread t=new Thread(r);
		t.start();
		
		// TODO Auto-generated method stub
		for(int i=0;i<10;i++) {
			System.out.println("Main Thread");
		}

	}

}

