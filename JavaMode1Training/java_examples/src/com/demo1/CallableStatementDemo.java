package com.demo1;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

public class CallableStatementDemo {

	public static void main(String[] args) {
		Connection con=null;
		try {
			con=DBConnection.getConnection();
			CallableStatement cst=con.prepareCall("{call insertProcedure(?,?)}");
			cst.setInt(1,12);
			cst.setString(2,"Maria");
			int no_of_rows=cst.executeUpdate();
			System.out.println(no_of_rows+" rows inserted successfully");
		}catch(Exception e) {
			System.out.println(e);
		}

	}

}
