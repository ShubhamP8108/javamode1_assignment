package com.car_inventory_project;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class CarViewMethods implements CarInterface{

	public int carCount() {
		int flag=0;
		try {
			Connection con;
			con = CarDBConnection.getConnection();
			Statement st=con.createStatement();
			ResultSet rs=st.executeQuery("select count(car_ID) from mycar");
			while (rs.next())
				flag=rs.getInt(1);
			con.close();
			return flag;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	public List<CarModel> displayCar() {
		List<CarModel> list=new ArrayList<CarModel>();
		CarModel car;
		Connection con;
		try {
		con = CarDBConnection.getConnection();
		Statement st=con.createStatement();
		ResultSet rs=st.executeQuery("select * from mycar;");
		while (rs.next()) {
			car=new CarModel(rs.getString(1),rs.getString(2),rs.getString(3),rs.getInt(4),rs.getDouble(5));
			list.add(car);
		}
		}catch(Exception e1) {
			e1.printStackTrace();
		}
		return list;	
	}
	public boolean carAdd(String carID, String make, String model, int year, double price) {
		int num = 0;
		boolean flag=false;
		try{
		Connection con=CarDBConnection.getConnection();
		Statement st=con.createStatement();
		num=st.executeUpdate("insert into mycar values('"+carID+"','"+make+"','"+model+"',"+year+","+price+")");
		con.close();
		}catch(SQLIntegrityConstraintViolationException sqe) {
			System.out.println("Entered car already exists");
		}catch(Exception se) {
			se.printStackTrace();
		}
		if (num==1)
			flag=true;
		else if (num==0)
			flag=false;
		return flag;
	}
	public boolean carDelete(String carID) {
		int num=0;
		boolean flag=false;
		try{
		Connection con=CarDBConnection.getConnection();
		Statement st=con.createStatement();
		num=st.executeUpdate("delete from mycar where car_ID='"+carID+"'");
		}catch(Exception se) {
			se.printStackTrace();
		}
		if (num==1)
			flag=true;
		else if (num==0)
			flag=false;
		return flag;
	}
	public boolean carUpdateMake(String carID, String make) {
		int num=0;
		boolean flag=false;
		try{Connection con=CarDBConnection.getConnection();
		Statement st=con.createStatement();
		num=st.executeUpdate("update mycar set car_make='"+make+"' where car_ID='"+carID+"'");
		}catch(Exception e) {
			e.printStackTrace();
		}
		if (num==1)
			flag=true;
		else if (num==0)
			flag=false;
		return flag;
	}
	public boolean carUpdateModel(String carID, String model) {
		int num=0;
		boolean flag=false;
		try{Connection con=CarDBConnection.getConnection();
		Statement st=con.createStatement();
		num=st.executeUpdate("update mycar set car_model='"+model+"' where car_ID='"+carID+"'");
		}catch(Exception e) {
			e.printStackTrace();
		}
		if (num==1)
			flag=true;
		else if (num==0)
			flag=false;
		return flag;

	}
	public boolean carUpdateYear(String carID, int year) {
		int num=0;
		boolean flag=false;
		try{Connection con=CarDBConnection.getConnection();
		Statement st=con.createStatement();
		num=st.executeUpdate("update mycar set car_year='"+year+"' where car_ID='"+carID+"'");
		}catch(Exception e) {
			e.printStackTrace();
		}
		if (num==1)
			flag=true;
		else if (num==0)
			flag=false;
		return flag;
	}
	public boolean carUpdatePrice(String carID, double price) {
		int num=0;
		boolean flag=false;
		try{Connection con=CarDBConnection.getConnection();
		Statement st=con.createStatement();
		num=st.executeUpdate("update mycar set car_sales_price='"+price+"' where car_ID='"+carID+"'");
		}catch(Exception e) {
			e.printStackTrace();
		}
		if (num==1)
			flag=true;
		else if (num==0)
			flag=false;
		return flag;
	}

	
	
}
