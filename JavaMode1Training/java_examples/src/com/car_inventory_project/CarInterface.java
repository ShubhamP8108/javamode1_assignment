package com.car_inventory_project;
import java.util.List;
public interface CarInterface {
	boolean carDelete(String carID);
	boolean carAdd(String carID,String make,String model,int year,double price);
	boolean carUpdateMake(String carID,String make);
	boolean carUpdateModel(String carID,String model);
	boolean carUpdateYear(String carID,int year);
	boolean carUpdatePrice(String carID,double price);
	List<CarModel> displayCar();
	int carCount();
}
