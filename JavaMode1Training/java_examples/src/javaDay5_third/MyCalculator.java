package javaDay5_third;
import java.util.Scanner;
import static java.lang.Math.pow;
class CustomException extends Exception {
	public CustomException(String excep){
		super(excep);
	}
}
public class MyCalculator {
	public static long power(int base,int exponent) {
			long answer=(long)pow((double)base, (double)exponent);
				return answer;
			}
	public static void main(String[] args) {
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter Base");
		int base=sc.nextInt();
		System.out.println("Enter Exponent");
		int exponent=sc.nextInt();
		if (base==0 && exponent==0) {
			try {
				throw new CustomException("n and p should not be zero.");
			} catch (CustomException e) {
				System.out.println(e);
			}
		}
		else if(base<0 || exponent<0) {
				try {
					throw new CustomException("n or p should not be negative.");
				} catch (CustomException e) {
					System.out.println(e);
				}
		}
		else {
		long result=power(base,exponent);
		System.out.println(result);
		}
	}
	}

