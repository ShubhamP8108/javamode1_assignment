package java_Day_6_third;
//This class implements Runnable interface
//It creates a thread with the class instance as argument
public class DemoThread1 implements Runnable{
	public void run() {
		for (int counter=1;counter<=10;counter++) {
			System.out.println("running "+Thread.currentThread().getName()+" in loop : "+counter);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	public static void main(String[] args) {
		DemoThread1 dthread=new DemoThread1();
		Thread thread1=new Thread(dthread);
		Thread thread2=new Thread(dthread);
		Thread thread3=new Thread(dthread);
		thread1.start();
	    thread2.start();
		thread3.start();
	}
}