package java_Day_6_first;
//This program demonstrates the use of getName,setName and sleep methods of Thread class
import java.time.LocalTime;
class ThreadDemo extends Thread{
	public void run() {
		System.out.print("The default name ");
		System.out.println(Thread.currentThread().getName());
		Thread.currentThread().setName("t1");
		System.out.println(Thread.currentThread().getName());
		Thread.currentThread().setName("MyThread");
		System.out.println(Thread.currentThread().getName());
		System.out.println(LocalTime.now());
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(LocalTime.now());
	}
}
public class MyThreadBasics {
	public static void main(String[] args) {
		ThreadDemo thread=new ThreadDemo();
		thread.start();
	}

}