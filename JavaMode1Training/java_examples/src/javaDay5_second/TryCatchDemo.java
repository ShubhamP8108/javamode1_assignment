package javaDay5_second;

import java.util.InputMismatchException;
import java.util.Scanner;

/*This program demonstrates the InputMismatchException. It occurs when our input
doesn't match with the variable data type we match it to.
It also demonstrates ArithmeticException. It occurs when we divide a number by zero.*/
public class TryCatchDemo {
	public static void main(String[] args) {
		try {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the numerator");
		int numerator=sc.nextInt();
		System.out.println("Enter the denominator");
		int denominator=sc.nextInt();
		int answer=numerator/denominator;
		System.out.println("The result is "+answer);
		sc.close();
		}
		catch (InputMismatchException ime) {
			System.out.println(ime);
		}
		catch (ArithmeticException ae) {
			System.out.println(ae);
		}
	}

}
