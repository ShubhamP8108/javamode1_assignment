 package java_Day_6_fourth;
//This class extends Thread class
//It creates a Thread and starts it when constructor is called
public class DemoThread1 extends Thread {
	public DemoThread1(){
		start();
	}
	public void run() {
		for (int counter=1;counter<=10;counter++) {
			System.out.println("running "+Thread.currentThread().getName()+" in loop : "+counter);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		new DemoThread1();
		new DemoThread1();
		new DemoThread1();
		
	}
}

