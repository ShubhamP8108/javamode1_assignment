package javaDay5_fifth;
//This program is used to calculate runrate using number of runs and overs.
//Most cases of exception that may generate have been handled. InputMismatchException, NumberFormatException and ArithmeticException
import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

public class RunRateCalculate{
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		DecimalFormat df=new DecimalFormat("#,###,##0.00");
		try {
		System.out.println("Enter the runs");
		int runs=Integer.parseInt(br.readLine());
		System.out.println("Enter the number of overs");
		int overs=Integer.parseInt(br.readLine());
		int runRate=runs/overs;
		System.out.println("Current Run Rate : "+ df.format(runRate));
		}catch(ArithmeticException ae) {
			System.out.println(ae);
		}catch(InputMismatchException ime) {
			System.out.println(ime);
		}catch(NumberFormatException nfe) {
			System.out.println(nfe);
		}
	}

}
