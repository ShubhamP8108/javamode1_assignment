package java_Day_6_fifth;

public class Number implements Runnable{
	public int number;
    Number(int number){
    	this.number=number;
    }
public static synchronized void display(int number) {
}
    public static void main(String[] args) throws InterruptedException {
        Number num1=new Number(3);
        Number num2=new Number(6);
        Number num3=new Number(9);
        Thread thread1=new Thread(num1);
        thread1.start();
        thread1.join();
        Thread thread2=new Thread(num2);
        thread2.start();
        thread2.join();
        Thread thread3=new Thread(num3);
        thread3.start();
        thread3.join();
        
        
    }
    @Override
    public synchronized void run() {
    	System.out.println(Thread.currentThread().getName()+" is" +Thread.currentThread().isAlive());
        System.out.println("Beginning of "+number+" multiples:");
        for(int i=1;i<=10;i++) {
            try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
                System.out.println(i*number);
        }
        System.out.println("End of "+number+" multiples:");
        System.out.println(Thread.currentThread().getName()+" is dead");
    }

 

}
