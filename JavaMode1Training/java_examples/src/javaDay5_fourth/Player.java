package javaDay5_fourth;
//This program demonstrates how to create a CustomException and throw it
import java.util.InputMismatchException;
import java.util.Scanner;
//This class extends Exception class and uses constructor for defining custom exception
class CustomException extends Exception {
	public CustomException (String excep){
		super(excep);
	}
}
public class Player {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		try {
		System.out.println("Enter the player name");
		String name=sc.nextLine();
		System.out.println("Enter the player age");
		int age=sc.nextInt();
		PlayerDemo player=new PlayerDemo(name,age);
		if (age<19) {
			try {
				throw new CustomException ("InvalidAgeRangeException");
			} catch (CustomException  e) {
				System.out.println(e);
			}
		}
		else {
			System.out.println("Player Name : " +player.getName());
			System.out.println("Player Age : " +player.getAge());
		}
		}catch(InputMismatchException ime) {
			System.out.println(ime);
		}
		sc.close();
		}

}