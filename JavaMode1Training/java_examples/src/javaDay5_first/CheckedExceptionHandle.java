package javaDay5_first;
import java.io.*;
//This program demonstrates a Checked Exception IOException. It is thrown at compile time when handling files.
public class CheckedExceptionHandle {
	public static void main(String[] args) throws IOException {
		try {
	FileReader fr=new FileReader("C:\\Users\\Shubham Pandey\\Documents\11608108_ShubhamPandey.txt");
	char[] mychar=new char[200];
	fr.read(mychar);
	System.out.println(mychar);
	fr.close();}
		catch(FileNotFoundException fnf) {
			System.out.println(fnf+ "  is thrown as file does not exist. ");
		}
	}
	}
