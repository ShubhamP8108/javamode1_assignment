<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- Required bootstrap css -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
   <!-- Optional JS but first jquery need to be add -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<title>Login Page</title>
<style>
.banner_image {
    padding-top: 110px;
 	padding-bottom: 50px;
 	background: url("https://blog.policyexpert.co.uk/wp-content/uploads/2013/11/iStock-628925952-1024x388.jpg") no-repeat center center;
 	background-size: cover;
    }
 .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    #submit{
    margin-left:80px;
    }
    #cancel{
    margin-left:50px;
    }
     footer {
    padding: 10px 0;
 	 background-color: #101010;
    color:#9d9d9d;
 	margin-top:30px;
 	bottom: 0;
 	width: 100%;
     
    }
</style>
<title>Insert title here</title>

</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">PetShop</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="Home.jsp">Home</a></li>
        <li><a href="MyPet.jsp">My Pet</a></li>
        <li class="active"><a href="#">Add Pet</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="LogoutServletController"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class ="banner_image">
            </div>
            <div class="container">
            <div class="row">
                <div class="col-xs-4 col-xs-offset-4">
                    <div class="panel panel-primary" >
                        <div class="panel-heading">
                            <h4>ADD PET</h4>
                        </div>
                        <div class="panel-body">
                            <form action="AddPetServletController" method="post">
                                <div class="form-group">
                                <label for="petname">PetName:</label>
                                <input type="text" class="form-control" placeholder="PetName" name="petname"  required = "true">
                                </div>
                                <div class="form-group">
                                <label for="petage">PetAge:</label>
                                <input type="text" class="form-control" placeholder="PetAge" name="petage" required = "true">
                                </div>
                                <label for="petplace">PetPlace:</label>
                                <input type="text" class="form-control" placeholder="PetPlace" name="petplace" required = "true">
                                </div>
                               <input type="submit" id="submit" value="Add Pet" class="btn btn-primary">
                                <a href="DisplayHome.jsp" class="cancel" id="cancel" name="cancel" class="btn btn-primary">Cancel</a><br>
                             </form><br/>
                </div>
            </div>
        </div>
 <footer>
            	<div class="container">
               		 <center>
                    <p>Copyright � PetPeers All Rights Reserved and Contact Shubham Pandey</p>
                	</center>
               </div>
 </footer>
</body>
</html>