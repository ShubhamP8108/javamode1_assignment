<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- Required bootstrap css -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
   <!-- Optional JS but first jquery need to be add -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<title>Insert title here</title>
<style>
th,td{
    border-width:1px;
    border-style:solid;
    border-color:#a4a4a4;
    height:30px;
    padding:15px;
   }
th{
    text-align:left;
     background-color:#ece
}
 .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
     footer {
    padding: 10px 0;
 	background-color: #101010;
    color:#9d9d9d;
 	margin-top:30px;
 	bottom: 0;
 	width: 100%;   
    }
</style>
<title>Insert title here</title>
</head>
<body>
<%
    session.setAttribute("currentusername", session.getAttribute("username"));
%>
<sql:setDataSource
        var="myDS"
        driver="com.mysql.cj.jdbc.Driver"
        url="jdbc:mysql://localhost:3306/petsdb"
        user="root" password="root123"/>   
    <sql:query var="CurrentUser" dataSource="${myDS}">
        SELECT userid from petuser where username="${sessionScope.currentusername}";
    </sql:query>
      <c:forEach var="user" items="${CurrentUser.rows}">
               <c:set value="${user}" var="currentuserid"/>
            </c:forEach>
    <sql:query var="listPets" dataSource="${myDS}">
        SELECT * from pet where petownerid=${currentuserid.userid};
    </sql:query>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">PetShop</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li ><a href="Home.jsp">Home</a></li>
        <li class="active"><a href="MyPet.jsp">My Pet</a></li>
        <li><a href="AddPet.jsp">Add Pet</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="LogoutServletController"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="table-responsive" align="center">
        <table border="2" cellpadding="10">
            <caption><h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MY PETS</h2></caption>
            <tr>
                <th>ID</th>
                <th>PETNAME</th>
                <th>PETAGE</th>
                <th>PETPLACE</th>
            </tr>
          
            <c:forEach var="mypet" items="${listPets.rows}">
                <tr>
                    <td><c:out value="${mypet.petid}" /></td>
                    <td><c:out value="${mypet.petname}" /></td>
                    <td><c:out value="${mypet.petage}" /></td>
                    <td><c:out value="${mypet.petplace}" /></td>
                </tr>
            </c:forEach>
        </table>
    </div>
 <footer>
            	<div class="container">
               		 <center>
                    <p>Copyright � PetPeers All Rights Reserved and Contact Shubham Pandey</p>
                	</center>
               </div>
 </footer>
</body>
</html>