<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- Required bootstrap css -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
   <!-- Optional JS but first jquery need to be add -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<title>Home Page</title>
<style>
#banner_content{
 position: relative;
 padding-top: 6%;
 padding-bottom: 6%;
 margin-top: 12%;
 margin-bottom: 12%;
 margin-left: 400px;
 background-color: rgba(0, 0, 0, 0.7);
 max-width: 350px;
}
#banner_image {
   padding-top: 94px;
   padding-bottom: 0;
 	text-align: center;
 	color: #f8f8f8;
 	background: url("https://blog.policyexpert.co.uk/wp-content/uploads/2013/11/iStock-628925952-1024x388.jpg") no-repeat center center;
 	background-size: cover;
    }
 .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
     footer {
    padding: 10px 0;
 	background-color: #101010;
    color:#9d9d9d;
 	margin-top:30px;
 	bottom: 0;
 	width: 100%;   
    }
</style>
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">PetShop</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
         <li><a href="Register.jsp"><span class="glyphicon glyphicon-user"></span>&nbsp;Register</a></li>
        <li><a href="Login.jsp"><span class="glyphicon glyphicon-log-in"></span>&nbsp;Login</a></li>
      </ul>
    </div>
  </div>
</nav>
  <div id="banner_image">
            <div class="container">
                <div id="banner_content">
                    <a href="#" class="btn btn-danger btn-lg active">BUY PETS</a>
                </div>
            </div>
        </div>
          <footer>
            	<div class="container">
               		 <center>
                    <p>Copyright � PetPeers All Rights Reserved and Contact Shubham Pandey</p>
                	</center>
            </div>
            </footer>
</body>
</html>