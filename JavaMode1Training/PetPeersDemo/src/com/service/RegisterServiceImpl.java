package com.service;

import com.dao.RegisterDaoImpl;
import com.dao.RegisterDaoIntf;

public class RegisterServiceImpl implements RegisterServiceIntf {
	public boolean registerUser(String username, String userpassword) {
		RegisterDaoIntf dao = (RegisterDaoIntf) new RegisterDaoImpl();
		return dao.registerUser(username, userpassword);
	}
	

}
