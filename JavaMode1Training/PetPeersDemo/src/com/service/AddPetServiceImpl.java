package com.service;

import com.dao.AddPetDaoImpl;
import com.dao.AddPetDaoIntf;

public class AddPetServiceImpl implements AddPetServiceIntf{
	public boolean addPet(String petname, int petage, String petplace) {
		AddPetDaoIntf dao= (AddPetDaoIntf) new AddPetDaoImpl();
		return dao.addPet(petname, petage, petplace);
	}

}
