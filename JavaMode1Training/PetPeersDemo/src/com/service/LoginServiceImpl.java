package com.service;

import com.dao.LoginDaoImpl;
import com.dao.LoginDaoIntf;
public class LoginServiceImpl implements LoginServiceIntf{
	public boolean validateLogin(String username, String userpassword) {
	    LoginDaoIntf dao = (LoginDaoIntf) new LoginDaoImpl();
		return dao.validateLogin(username, userpassword);
	}

}
