package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.service.LoginServiceImpl;
import com.service.LoginServiceIntf;
@WebServlet("/LoginServletController")
public class LoginServletController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 LoginServiceIntf service = new LoginServiceImpl();
		 response.setContentType("text/html");  
         PrintWriter out = response.getWriter();  
         String username=request.getParameter("username");  
         String userpassword=request.getParameter("userpassword");  
         if(service.validateLogin(username, userpassword))
         {    
        	  HttpSession session=request.getSession();
              session.setAttribute("username",username);
              RequestDispatcher rd=request.getRequestDispatcher("Home.jsp");  
              rd.forward(request,response);  
         }  
         else
         {  
              out.print("Sorry username and password not matched");  
              RequestDispatcher rd=request.getRequestDispatcher("Login.jsp");  
              rd.include(request,response);  
         }  
         out.close();  
	}

}
