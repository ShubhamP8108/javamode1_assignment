package com.controller;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.BuyPetDaoImpl;

@WebServlet("/BuyPetServletController")
public class BuyPetServletController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		String petid=request.getParameter("petid");
		int currentpetid=Integer.parseInt(petid);
        String userid=request.getParameter("userid");
        int currentuserid=Integer.parseInt(userid);
        BuyPetDaoImpl dao=new BuyPetDaoImpl();
        if(dao.updatePet(currentuserid, currentpetid)) {
        	RequestDispatcher rd=request.getRequestDispatcher("DisplayHome.jsp");  
            rd.include(request,response); 
        }
       
	}

}
