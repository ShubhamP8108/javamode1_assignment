package com.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.service.AddPetServiceImpl;
import com.service.AddPetServiceIntf;
@WebServlet("/AddPetServletController")
public class AddPetServletController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AddPetServiceIntf service = new AddPetServiceImpl();
		response.setContentType("text/html");  
        PrintWriter out = response.getWriter();  
        String petname = request.getParameter("petname");  
        int petage = Integer.parseInt(request.getParameter("petage"));
        String petplace=request.getParameter("petplace");
        if(service.addPet(petname, petage, petplace))
        {  
             RequestDispatcher rd=request.getRequestDispatcher("DisplayHome.jsp");  
             rd.forward(request,response);
        }  
        else
        {  
             out.print("Petname Akready Exist");  
             RequestDispatcher rd=request.getRequestDispatcher("AddPet.jsp");  
             rd.include(request,response); 
        }  
        out.close();  
	}

}
