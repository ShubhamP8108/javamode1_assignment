package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.service.RegisterServiceImpl;
import com.service.RegisterServiceIntf;
@WebServlet("/RegisterServletController")
public class RegisterServletController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RegisterServiceIntf service = new RegisterServiceImpl();
		response.setContentType("text/html");  
        PrintWriter out = response.getWriter();  
        String username = request.getParameter("username");  
        String userpassword = request.getParameter("userpassword");   
        if(service.registerUser(username, userpassword))
        {  
             RequestDispatcher rd=request.getRequestDispatcher("Login.jsp");  
             rd.forward(request,response);
        }  
        else
        {  
             out.print("Username Already Exist");  
             RequestDispatcher rd=request.getRequestDispatcher("Register.jsp");  
             rd.include(request,response); 
        }  
        out.close();  
	}

}
