package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class AddPetDaoImpl implements AddPetDaoIntf{
	public boolean addPet(String petname, int petage, String petplace) {
		boolean status=false;
        try
        {  
      	  Class.forName("com.mysql.cj.jdbc.Driver");
    	  Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/petsdb","root","root123");
    	  PreparedStatement ps=con.prepareStatement("insert into pet(petname,petage,petplace) values(?,?,?)");  
    	  ps.setString(1,petname);  
          ps.setInt(2, petage);
          ps.setString(3,petplace); 
          int i = ps.executeUpdate();  
          if(i>0) {
          	status=true;
          }
         
        }
        catch(Exception e)
        {
      	  e.printStackTrace();
        }  
        return status;  
	}
	

}
