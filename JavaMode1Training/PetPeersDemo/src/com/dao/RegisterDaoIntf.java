package com.dao;

public interface RegisterDaoIntf {
	boolean registerUser(String username, String userpassword);
}
