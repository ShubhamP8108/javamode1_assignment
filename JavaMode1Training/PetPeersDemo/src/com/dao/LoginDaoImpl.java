package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class LoginDaoImpl implements LoginDaoIntf{
	public boolean validateLogin(String username, String userpassword) {
        boolean status=false;
        try
        {  
      	     Class.forName("com.mysql.cj.jdbc.Driver");
    	     Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/petsdb","root","root123");
             PreparedStatement ps=con.prepareStatement("select * from petuser where username=? and userpassword=?");  
             ps.setString(1,username);  
             ps.setString(2,userpassword);  
             ResultSet rs=ps.executeQuery();  
             status=rs.next();
             System.out.println(status);
        }
        catch(Exception e)
        {
             System.out.println(e);
        }  
        return status;
	}

}
