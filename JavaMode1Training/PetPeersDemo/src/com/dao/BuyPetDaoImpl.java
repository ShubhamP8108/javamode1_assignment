package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class BuyPetDaoImpl {
	public boolean updatePet(int userid,int petid) {
		boolean status=false;
        try
        {  
      	  Class.forName("com.mysql.cj.jdbc.Driver");
    	  Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/petsdb","root","root123");
    	  PreparedStatement ps=con.prepareStatement("update pet set petownerid=? where petid=?");  
    	  ps.setInt(1,userid);  
          ps.setInt(2, petid);
          int i = ps.executeUpdate();  
          if(i>0) {
          	status=true;
          }
         
        }
        catch(Exception e)
        {
      	  e.printStackTrace();
        }  
        return status;  
	}
	
}
