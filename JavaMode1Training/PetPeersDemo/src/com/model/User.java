package com.model;

import java.util.Set;

public class User {
 private int userid;
 private String username;
 private String userpassword;
 private Set pets;
public int getUserid() {
	return userid;
}
public void setUserid(int userid) {
	this.userid = userid;
}
public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getUserpassword() {
	return userpassword;
}
public void setUserpassword(String userpassword) {
	this.userpassword = userpassword;
}
public Set getPets() {
	return pets;
}
public void setPets(Set pets) {
	this.pets = pets;
}
}
