package com.model;

public class Pet {
	private int petid;
	private String petname;
	private int petage;
	private String petplace;
    private User user;
	public int getPetid() {
		return petid;
	}
	public void setPetid(int petid) {
		this.petid = petid;
	}
	public String getPetname() {
		return petname;
	}
	public void setPetname(String petname) {
		this.petname = petname;
	}
	public int getPetage() {
		return petage;
	}
	public void setPetage(int petage) {
		this.petage = petage;
	}
	public String getPetplace() {
		return petplace;
	}
	public void setPetplace(String petplace) {
		this.petplace = petplace;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

}
