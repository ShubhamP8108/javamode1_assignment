package com.inheritance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import lombok.Data;
@Data
@Entity
@PrimaryKeyJoinColumn(name="EID")
public class RegularEmployee extends Employee {
	@Column(name="salary")    
	private float salary;  
	  
	@Column(name="bonus")     
	private int bonus;  
}
