package com.inheritance;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import lombok.Data;
@Data
@Entity
@Table(name="contractemployee")
@PrimaryKeyJoinColumn(name="EID")
public class ContractEmployee extends Employee{
    
  @Column(name="pay_per_hour")  
  private float pay_per_hour;  
    
  @Column(name="contract_duration")  
  private String contract_duration;  
}
