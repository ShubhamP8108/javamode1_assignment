package com.demo;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.model.Answers;
import com.model.Questions;
import com.model.Student;

public class HibernateMain {

	public static void main(String[] args) {
		Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
		Questions que=new Questions();
		que.setQuestion_name("Who founded java?");
		Answers ans1=new Answers();
		ans1.setAnswer_name("James Gosling");//answer1
		Answers ans2=new Answers();
		ans2.setAnswer_name("GreenTalk System");//answer2
		List<Answers> ansList=new ArrayList<Answers>();//creating list of answers for the question
		ansList.add(ans1);
		ansList.add(ans2);
		que.setAnswer(ansList);//setting answers list to question
		session.save(que);
		Transaction txn=session.beginTransaction();
		txn.commit();
	}

}
