package com.demo;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.model.Department;

import java.util.List;

import org.hibernate.Query;

public class DepartmentMain {

	public static void main(String[] args) {
		Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
		Department dept=new Department(1,"ECE","Tenali");
        session.save(dept);
        Transaction txn=session.beginTransaction();
        txn.commit();
        /*Query query=session.getNamedQuery("findDepartmentByName");*/
        Query query=session.getNamedQuery("findDepartmentByName");
        query.setParameter("name","ECE");
        List<Department> ls=query.list();
        ls.forEach(dept1->System.out.println(dept1));

	}

}
