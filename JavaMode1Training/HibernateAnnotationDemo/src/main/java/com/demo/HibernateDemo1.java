package com.demo;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.model.Student;

public class HibernateDemo1 {

	public static void main(String[] args) {
		Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
		Student st=new Student(1,"revathi",33);
		Transaction txn=session.beginTransaction();
		session.save(st);
		txn.commit();
	}

}
