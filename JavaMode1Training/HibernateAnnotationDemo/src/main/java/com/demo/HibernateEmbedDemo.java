package com.demo;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.model.Address;
import com.model.Person;

public class HibernateEmbedDemo {

	public static void main(String[] args) {
		Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
		Person person=new Person();
		person.setFirstname("Tony");
		person.setMiddlename("kakkar");
		person.setLastname("Misto");
		Address address=new Address();
		address.setState("Delhi");
		address.setCity("Dwarka");
		address.setCountry("India");
		person.setAddress(address);
		session.save(person);
		Transaction txn=session.beginTransaction();
		txn.commit();

	}

}
