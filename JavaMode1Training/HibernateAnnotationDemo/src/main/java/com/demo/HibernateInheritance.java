package com.demo;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.inheritance.ContractEmployee;
import com.inheritance.Employee;
import com.inheritance.RegularEmployee;

public class HibernateInheritance {

	public static void main(String[] args) {
	Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
	Employee emp=new Employee();    
	emp.setName("Gaurav Chawla");    
        
    RegularEmployee regemp=new RegularEmployee();    
    regemp.setName("Vivek Kumar");    
    regemp.setSalary(50000);    
    regemp.setBonus(5);    
        
    ContractEmployee conemp=new ContractEmployee();    
    conemp.setName("Arjun Kumar");    
    conemp.setPay_per_hour(1000);    
    conemp.setContract_duration("15 hours");  
	
    session.save(emp);
    session.save(regemp);
    session.save(conemp);
	Transaction txn=session.beginTransaction();
	txn.commit();

	}

}
