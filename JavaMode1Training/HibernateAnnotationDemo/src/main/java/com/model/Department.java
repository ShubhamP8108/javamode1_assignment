package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name="department")
/*@NamedQueries(
{
    @NamedQuery(name="findDepartmentByName",query="from Department d where d.deptname=:name")

})*/
@NamedNativeQueries(
{
    @NamedNativeQuery(name="findDepartmentByName",query="select * from department where deptname=:name", resultClass=Department.class)

})

public class Department {
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
     private int deptId;
     private String deptName;
     private String deptLocation;
}
