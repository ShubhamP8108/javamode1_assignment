package com.model;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import lombok.Data;
import lombok.NoArgsConstructor;
@NoArgsConstructor
@Data
@Entity
public class Questions {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int question_id;
	private String question_name;
	@JoinTable(name="JoinedTable",joinColumns=@JoinColumn(name="question_id"),inverseJoinColumns=@JoinColumn(name="answer_id"))
	@OneToMany(cascade=CascadeType.ALL)
	//@JoinColumn(name="question_id",referencedColumnName="question_id")
	private List<Answers> answer;
}
