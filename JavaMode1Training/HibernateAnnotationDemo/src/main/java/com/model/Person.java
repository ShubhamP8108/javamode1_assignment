package com.model;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name="PersonDetails")
public class Person {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int personID;
private String firstname;
private String middlename;
private String lastname;
@Embedded
private Address address;
}
