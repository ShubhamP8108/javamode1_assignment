package com.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@AllArgsConstructor
@Data
@NoArgsConstructor
@Entity
@Table(name="tbl_std")
public class Student {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int stdid;
    @Column(name="Firstname")
    private String stdname;
    private int stdage;
}
