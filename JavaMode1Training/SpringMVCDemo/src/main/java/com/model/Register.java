package com.model;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
@Entity
@Table(name="RegisterNew")
public class Register {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int userId;
@NotEmpty(message="Username should not be empty")
private String username;
@NotEmpty(message="Password should not be empty")
private String password;
@NotEmpty(message="Languages should be checked")
private String languages;
@NotEmpty(message="Gender should be selected")
private String gender;
@NotNull(message="DateOfBirth need to be picked up")
@DateTimeFormat(pattern = "yyyy-MM-dd")
//@Temporal(TemporalType.TIMESTAMP)
private Date dateOfBirth;
@NotEmpty(message="Country should be selected")
private String country;
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public Date getDateOfBirth() {
	return dateOfBirth;
}
public void setDateOfBirth(Date dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
}
public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getLanguages() {
	return languages;
}
public void setLanguages(String languages) {
	this.languages = languages;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}

}
