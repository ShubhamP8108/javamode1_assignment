<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration Page</title>
<style>
.error{
color: #ff0000;
font-weight: bold;
}
</style>
</head>
<body>
<form:form action="saveRegister" modelAttribute="register">
Username:<form:input path="username"/><form:errors cssClass="error" path="username"></form:errors><br/>
Password:<form:password path="password"/><form:errors cssClass="error" path="password"></form:errors><br/>
DOB:<form:input type="date" path="dateOfBirth" /><form:errors cssClass="error" path="dateOfBirth"></form:errors><br>
Country:<form:select path="country">
    <form:option value="" label="--Please Select Country--"/>
    <form:option value="US" label="United States"/>
    <form:option value="IT" label="Italy"/>
    <form:option value="FR" label="France"/>
</form:select><form:errors cssClass="error" path="country"></form:errors><br>
Languages:
C<form:checkbox path="languages" value="C"/>
C++<form:checkbox path="languages" value="C++"/>
Java<form:checkbox path="languages" value="Java"/><form:errors cssClass="error" path="languages"></form:errors><br/>
Gender:
Male:<form:radiobutton path="gender" value="Male"/>
Female<form:radiobutton path="gender" value="Female"/><form:errors cssClass="error" path="gender"></form:errors><br/>
<input type="submit" value="Submit">
<input type="reset" value="Reset">
</form:form>
</body>
</html>