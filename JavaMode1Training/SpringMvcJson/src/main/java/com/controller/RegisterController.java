package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.model.Register;
import com.service.RegisterServiceIntf;
@RestController
//@Controller
public class RegisterController {
	@Autowired
	RegisterServiceIntf service;
	//@RequestMapping(value="saveRegister",method=RequestMethod.POST)
	@PostMapping("saveRegister")
	public void saveRegister(@RequestBody Register register) {
		service.saveRegister(register);
	}
	//@RequestMapping(value="fetchUsers" ,method=RequestMethod.GET)
	@GetMapping("fetchUsers")
	//@ResponseBody
	public List<Register> getUsers()
	{
	List<Register> ls = service.getUsers();
	return ls;
	}	
	@PutMapping("updateRegister/{userId}")
	public void updateRegister(@RequestBody Register register,int userId) {
		service.updateRegister(register,userId);
	}
	@DeleteMapping("deleteRegister/{userId}")
	public void deleteRegister(@RequestBody String userId) {
		int ID=Integer.parseInt(userId);
		service.deleteRegister(ID);
		
	}
}
