package com.dao;

import java.util.List;

import com.model.Register;

public interface RegisterDaoIntf {

	void saveRegister(Register register);
	public List<Register> getUsers();
	public void deleteRegister(int userId);
	public void updateRegister(Register register,int userId);

}
