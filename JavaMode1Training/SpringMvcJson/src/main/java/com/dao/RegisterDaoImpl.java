package com.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.model.Register;
@Repository

public class RegisterDaoImpl implements RegisterDaoIntf {
	@Autowired
	SessionFactory sessionFactory;
	public void saveRegister(Register register) {
	sessionFactory.openSession().save(register);
	}
	public List<Register> getUsers() {
		List<Register> ls=sessionFactory.openSession().createQuery("from Register").list();
		return ls;
	}
	public void updateRegister(Register register,int userId) {
	/*	sessionFactory.openSession().update(register);*/
		Query query=(Query) sessionFactory.openSession().createSQLQuery("update register set languages=:language where userId=:userId");
		query.setParameter("userId", userId);
		query.setParameter("languages",register.getLanguages());
		query.executeUpdate();
		
	}
	public void deleteRegister(int userId) {
	/*	Register user=(Register) sessionFactory.openSession().get(Register.class, userId);
		sessionFactory.openSession().delete(user);*/
		Query query=(Query) sessionFactory.openSession().createSQLQuery("delete from register where userId=:userId");
		query.setParameter("userId", userId);
		query.executeUpdate();
		
	}

}



//@Repository
/*public class RegisterDaoImpl implements RegisterDaoIntf {
	@Autowired
	JdbcTemplate jdbcTemplate;
	public void saveRegister(Register register) {
		java.sql.Date sqlDate=new java.sql.Date(register.getDateOfBirth().getTime());
		jdbcTemplate.update("insert into register values('"+register.getUsername()+"','"+register.getPassword()+"','"+register.getCountry()+"','"+register.getLanguages()+"','"+register.getGender()+"','"+sqlDate+"')");
		System.out.println("Success");
	}

}*/