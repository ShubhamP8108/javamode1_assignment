package com.demo;

import java.util.List;
import java.util.Scanner;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import com.model.Employee;
public class MyFirstHibernateDemo {
	public static void main(String[] args) {
    Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
    Criteria criteria=session.createCriteria(Employee.class);
    criteria.setProjection(Projections.property("name"));
    List<String> ls=criteria.list();
    ls.forEach(empId->System.out.println(empId));
	}
}

//Restrictions(Criteria)
/*Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
Criteria criteria=session.createCriteria(Employee.class);
criteria.add(Restrictions.gt("id",2));
List<Employee> ls=criteria.list();
ls.forEach(empId->System.out.println(empId));*/
//createcriteria(HSQL)
/*Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
Criteria criteria=session.createCriteria(Employee.class);
List<Employee> ls=criteria.list();
ls.forEach(empId->System.out.println(empId));*/

//unique result
/*Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
Employee emp=new Employee(1,"revathi",33);
Scanner scanner=new Scanner(System.in);
System.out.println("Enter id");
int id=Integer.parseInt(scanner.nextLine());
Query query=session.createSQLQuery("select*from tblemp where id=:emp").addEntity(Employee.class);
query.setParameter("emp",id);
Employee emp1=(Employee)query.uniqueResult();
System.out.println(emp1.getAge());*/

//Native Sql
/*Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
Employee emp=new Employee(1,"revathi",33);
Scanner scanner=new Scanner(System.in);
System.out.println("Enter id");
int id=Integer.parseInt(scanner.nextLine());
Query query=session.createSQLQuery("select*from tblemp where id=:emp").addEntity(Employee.class); //Native SQL
query.setParameter("emp",id);
List<Object> ls=query.list();
ls.forEach(empId->System.out.println(empId));*/
/*Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
Employee emp=new Employee(1,"revathi",33);
Query query=session.createQuery("select e.id from Employee e");
List<Integer> ls=query.list();
ls.forEach(empId->System.out.println(empId));*/

//save
/*Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
Employee emp=new Employee(1,"revathi",33);
Transaction txn=session.beginTransaction();
session.save(emp);
txn.commit();*/


//update
/*Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
Transaction txn=session.beginTransaction();
 Employee emp=(Employee) session.get(Employee.class,1);
if(emp!=null) {
	emp.setName("shubham");
	session.update(emp);
	txn.commit();
}
else {
	System.out.println("No such Employee");
}*/

//delete
/*Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
Transaction txn=session.beginTransaction();
 Employee emp=(Employee) session.get(Employee.class,1);
if(emp!=null) {
	emp.setName("shubham");
	session.update(emp);
	session.delete(emp);
	txn.commit();
}
else {
	System.out.println("No such Employee");
}*/