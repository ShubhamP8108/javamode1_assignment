package com.filter;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

@WebFilter(urlPatterns="/AfterFilterServlet")
public class MyFilterDemo1 implements Filter {
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		PrintWriter out=response.getWriter();
		String username=request.getParameter("username");
		String password=request.getParameter("password");
		if(password.equals("") && username.equals("")) {
			out.println("Please enter both username and password");
			response.setContentType("text/html");
			RequestDispatcher rd=request.getRequestDispatcher("filterdemo1.html");
			rd.include(request, response);
		}
		else if(password.equals("admin") && username.equals(username)) {
			chain.doFilter(request, response);
		}
		else {
			out.println("Wrong username or password");
			response.setContentType("text/html");
			RequestDispatcher rd=request.getRequestDispatcher("filterdemo1.html");
			rd.include(request, response);
		}
	}


}
