package com.servletprograms;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
@WebServlet("/DateTimeServlet")
public class DateTimeServlet extends GenericServlet {
	private static final long serialVersionUID = 1L;
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		//setting response content type
        response.setContentType("text/html");
        //getting stream object
        PrintWriter out = response.getWriter();
        //written request processing logic
        Date date = new Date();
        out.println("<h2>"+"Current Date & Time:\t "+date.toString()+"</h2>");
        //close stream object
        out.close();
	}

}
