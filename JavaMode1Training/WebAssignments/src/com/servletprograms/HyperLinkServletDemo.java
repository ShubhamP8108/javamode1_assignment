package com.servletprograms;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet("/HyperLinkServletDemo")
public class HyperLinkServletDemo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//setting response content type
        response.setContentType("text/html");
        //getting printWrite object
        PrintWriter out = response.getWriter();
        //written request processing logic to generate wish message
        Calendar cal = Calendar.getInstance();
        //getting current hours of the day
        int hour = cal.get(Calendar.HOUR_OF_DAY);//24 hrs format
        //generating wish message
        if(hour<12)
             out.println("Good Morning!!");
        else if (hour < 16)
             out.println("Good afternoon");
        else if(hour<20)
             out.println("Good evening");
        else
             out.println("Good night");

        out.println("<br><br><a href= 'http://localhost:8081/WebAssignments/hyperlink.html'>Home</a>");
        //closing stream object
        out.close();
	}

}
