package com.servletprograms;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@WebServlet("/ServletContextDemo")
public class ServletContextDemo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");  
		PrintWriter out=response.getWriter();  
		//creating ServletContext object  
		ServletContext context=getServletContext();     
		String url=context.getInitParameter("URL");  
		String username=context.getInitParameter("UserName");  
		String password=context.getInitParameter("Password");  ;
		out.println("<html><body>");
        out.println("<h1>URL for connecting mysql database is=\t"+url+"</h1>");
        out.println("<h1>Username for connecting mysql database is=\t"+username+"</h1>");
        out.println("<h1>Password for connecting mysql database is=\t"+password+"</h1>");
        out.println("</body></html>");
		out.close(); 
	}

}
