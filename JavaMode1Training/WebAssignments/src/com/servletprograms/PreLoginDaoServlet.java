package com.servletprograms;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.LoginDao;
@WebServlet("/PreLoginDaoServlet")
public class PreLoginDaoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html");  
         PrintWriter out = response.getWriter();  
         String name=request.getParameter("username");  
         String pass=request.getParameter("userpass");  
         if(LoginDao.validate(name, pass))
         {  
              RequestDispatcher rd=request.getRequestDispatcher("WelcomeServlet");  
              rd.forward(request,response);  
         }  
         else
         {  
              out.print("Sorry username or password error");  
              RequestDispatcher rd=request.getRequestDispatcher("login.html");  
              rd.include(request,response);  
         }  
         out.close();  
	}

}
