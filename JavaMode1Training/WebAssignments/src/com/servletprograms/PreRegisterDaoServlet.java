package com.servletprograms;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.dao.RegisterDao;
@WebServlet("/PreRegisterDaoServlet")
public class PreRegisterDaoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");  
        PrintWriter out = response.getWriter();  
        String name = request.getParameter("userName");  
        String pwd = request.getParameter("userPass");  
        String email = request.getParameter("userEmail");
        String mobile = request.getParameter("userMobile");
        String dob = request.getParameter("userDOB");  
        String gender = request.getParameter("gender");  
        String country =request.getParameter("userCountry");  
        if(RegisterDao.registerUser(name, pwd, email, mobile, dob, gender, country))
        {  
             RequestDispatcher rd=request.getRequestDispatcher("DisplayRegisterUser.jsp");  
             rd.forward(request,response);
        }  
        else
        {  
             out.print("Please Try Again......");  
             RequestDispatcher rd=request.getRequestDispatcher("register.html");  
             rd.include(request,response); 
        }  
        out.close();  
	}

}
