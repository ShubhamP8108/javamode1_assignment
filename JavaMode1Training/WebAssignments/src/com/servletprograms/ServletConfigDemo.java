package com.servletprograms;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//@WebServlet("/ServletConfigDemo")
public class ServletConfigDemo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  response.setContentType("text/html");
	      PrintWriter out = response.getWriter();
	      ServletConfig sc=getServletConfig();
          int number1=Integer.parseInt(sc.getInitParameter("N1"));
          int number2=Integer.parseInt(sc.getInitParameter("N2"));
          int result=number1+number2;
          out.println("<html><body>");
          out.println("<h1>Number1 value is:\t"+number1+"</h1>");
          out.println("<h1>Number2 value is:\t"+number2+"</h1>");
          out.println("<h1>Result is:\t"+result+"</h1>");
          out.println("</body></html>");   
		
	}

}
