package com.servletprograms;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/CountingVisitsServletDemo")
public class CountingVisitsServletDemo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private int myHittingCounter;
	public void init(ServletConfig config) throws ServletException {
		myHittingCounter = 0;
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			  //setting response content type
        	  response.setContentType("text/html");
		      PrintWriter out =  response.getWriter();
	          out.println("<form><fieldset style='width:15%'>");
	          out.println("<h3>Welcome to my website !</h3><hr>");
	          out.println("You are visitor number: "+ (++myHittingCounter));
	          out.println("</fieldset></form>");
	}

}
