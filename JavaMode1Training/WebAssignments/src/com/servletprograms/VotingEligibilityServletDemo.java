package com.servletprograms;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet("/VotingEligibilityServletDemo")
public class VotingEligibilityServletDemo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//set response content type
        response.setContentType("text/html");
        //getting printWrite object
        PrintWriter out = response.getWriter();
        //reading form data from page as request parameter
        String name = request.getParameter("name");
        int age = Integer.parseInt(request.getParameter("age"));
        if (age>=18)
        {
        	out.println("<font color='green' size='4'>"+name+" you are eligible to vote</font>");
        }
        else
        	out.println("<font color='red' size='4'>"+name+" you are not eligible to vote</font>");
        //adding hyper-link to dynamic page
        out.println("<br><br><a href= 'votingdemo.html'>Home</a>");
        //close the stream
        out.close();
	}

}
