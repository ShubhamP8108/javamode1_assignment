package com.servletprograms;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet("/AutoRefreshDemo")
public class AutoRefreshDemo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  // Setting refresh, auto-loading time as 5 seconds
        response.setIntHeader("Refresh", 5);
        // Setting response content type
        response.setContentType("text/html");
        // Getting current time
        Calendar calendar = new GregorianCalendar();
        String am_pm;
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        if(calendar.get(Calendar.AM_PM) == 0)
             am_pm = "AM";
        else
             am_pm = "PM";

        String currentTime = hour+":"+ minute +":"+ second +" "+ am_pm;
  
        PrintWriter out = response.getWriter();
    
        out.println("<h1 align='center'>Auto Refresh Page</h1><hr>");
        out.println("<h3 align='center'>Current time: "+currentTime+"</h3>");
	}

}
