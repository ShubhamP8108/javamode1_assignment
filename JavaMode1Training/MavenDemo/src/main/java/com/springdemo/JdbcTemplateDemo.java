package com.springdemo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.dao.EmployeeDao;
import com.pojo.Employee;
import com.pojo.LifeCycleDemo;
public class JdbcTemplateDemo {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext context=new ClassPathXmlApplicationContext("Beans.xml");
		EmployeeDao dao=context.getBean("emp",EmployeeDao.class);
        Employee emp=new Employee(2,"shubham");
        Employee emp1=new Employee(3,"ram");
        dao.insertEmployee(emp);
        dao.insertEmployee(emp1);
	}

}
