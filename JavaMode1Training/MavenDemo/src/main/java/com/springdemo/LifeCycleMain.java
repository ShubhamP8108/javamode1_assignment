package com.springdemo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pojo.LifeCycleDemo;

public class LifeCycleMain {

	public static void main(String[] args) {
		AbstractApplicationContext context=new ClassPathXmlApplicationContext("Beans.xml");
		LifeCycleDemo lcd=context.getBean("lifecycle",LifeCycleDemo.class);
		System.out.println(lcd.getMessage());  
		//context.close();  //destroy method called
		context.registerShutdownHook();
		 
	}

}
