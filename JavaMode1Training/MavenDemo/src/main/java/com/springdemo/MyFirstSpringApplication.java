package com.springdemo;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.pojo.Employee;
import com.pojo.Hello;
import com.pojo.Student;

public class MyFirstSpringApplication {//eager loading and lazy loading

	public static void main(String[] args) {
		    ApplicationContext context=new ClassPathXmlApplicationContext("Beans.xml");
		    Student st=context.getBean("st",Student.class); //calling init method
	        System.out.println(st.getId()+" "+st.getName()+" "+st.getAddress().getCity()+" "+st.getAddress().getState());
            
	}

}


/* Hello hello=context.getBean("emp",Hello.class);
hello.setMessage("Hi Welcome to hcl");
System.out.println(hello.getMessage());
Hello hello1=context.getBean("emp",Hello.class);
System.out.println(hello1.getMessage());*/
/*Resource resource=new ClassPathResource("Beans.xml");
BeanFactory factory=new XmlBeanFactory(resource);
Employee emp=factory.getBean("emp",Employee.class);
emp.display();*/
/*ApplicationContext context=new ClassPathXmlApplicationContext("Beans.xml");
Employee emp=context.getBean("emp",Employee.class);
emp.display();
Student st=context.getBean("st",Student.class);
System.out.println(st);*/