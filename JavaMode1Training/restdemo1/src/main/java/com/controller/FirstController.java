package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.model.Employee;

@RestController
public class FirstController {
	@RequestMapping(value="one")    //endpoint
     public String sayHello() {
    	 return "Welcome to spring rest";
     }
	@RequestMapping(value="emp",method=RequestMethod.GET)
	public Employee getEmployee() {
		return new Employee(10,"Ten",1010.10f);
	}
	@RequestMapping(value="emppath/{abc}",method=RequestMethod.GET)
	public Employee getEmployeeById(@PathVariable("abc") int arg) {
		Employee employee=null;
		if(arg == 20) {
			employee= new Employee(20,"Twenty",2020.20f);
		}
		if(arg == 30 ) {
			employee= new Employee(30,"Thirty",3030.30f);
		}
		return employee;
	}
	@RequestMapping(value="empall",method=RequestMethod.GET)
	public List<Employee> getAllEmployees() {
		Employee employee1=new Employee(20,"Twenty",2020.20f);
		Employee employee2=new Employee(30,"Thirty",3030.30f);
		Employee employee3=new Employee(40,"Forty",4040.40f);
		List<Employee> employees = new ArrayList<Employee>();
		employees.add(employee1);
		employees.add(employee2);
		employees.add(employee3);
		return employees;
	}
	@RequestMapping(value="empcreate",method=RequestMethod.POST)
	public Employee addEmployee(@RequestBody Employee employee) //oop
	{
		System.out.println("Server Side emp id:"+employee.getEmpNo());
		System.out.println("Server Side emp name:"+employee.getEmpName());
		System.out.println("Server Side emp salary:"+employee.getSalary());
		return employee;
	}
	@RequestMapping(value="empupdate",method=RequestMethod.PUT)
	public Employee updateEmployee(@RequestBody Employee employee) //oop
	{
		System.out.println("Server Side update emp id:"+employee.getEmpNo());
		System.out.println("Server Side update emp name:"+employee.getEmpName());
		System.out.println("Server Side update emp salary:"+employee.getSalary());
		employee.setSalary(5000);
		return employee;
	}
    @RequestMapping(value = "empdelete/{abc}", method = RequestMethod.DELETE)
    public boolean deleteEmployeeById(@PathVariable("abc") int arg) {
        System.out.println(" employee no " + arg + "is deleted");
        return true;
    }
	
}
