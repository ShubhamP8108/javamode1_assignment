Name : Shubham Pandey
SAPID: 51937504


1.
package Day4;

public abstract class Shape {
	protected String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
public Shape(String name){
	this.name=name;
}
abstract Float calculateArea();
}

package Day4;

public class Square extends Shape{
	private Integer side;

	public Integer getSide() {
		return side;
	}

	public void setSide(Integer side) {
		this.side = side;
	}
public Float calculateArea(){
	Float area=Float.valueOf((float)side*side);
	return area;
	
}
public Square(String name,Integer side){
	super(name);
	this.side=side;
}
}
package Day4;

public class Circle extends Shape {
	private Integer radius;
	
	public Integer getRadius() {
		return radius;
	}
	public void setRadius(Integer radius) {
		this.radius = radius;
	}
	public Float calculateArea(){
		Float area=Float.valueOf(radius*radius*3.14f);
		return area;
	}
public Circle(String name,Integer radius){
	super(name);
	this.radius=radius;
}
}

package Day4;

public class Rectangle extends Shape{
	private Integer length;
	private Integer width;
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	public Integer getWidth() {
		return width;
	}
	public void setWidth(Integer width) {
		this.width = width;
	}
	public Float calculateArea(){
		Float area=Float.valueOf((float)length*width);
		return area;
	}
	public Rectangle(String name,Integer length,Integer width){
		super(name);
		this.length=length;
		this.width=width;
	}
	

}

package Day4;

import java.util.Scanner;

public class Main {
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the shape name");
		System.out.println("1.Circle\n2.Rectangle\n3.Square");
		String choice=sc.next();
		if (choice.equals("Circle")){
			System.out.println("Enter the radius");
			int raidus=sc.nextInt();
			Circle cir1=new Circle("Cir1",raidus);
			System.out.printf("%.2f",cir1.calculateArea());
		}
		else if (choice.equals("Rectangle")){
			System.out.println("Enter the length");
			int length=sc.nextInt();
			System.out.println("Enter the width");
			int width=sc.nextInt();
			Rectangle rect1=new Rectangle("Rect1",length,width);
			System.out.printf("%.2f",rect1.calculateArea());
		}
		else if (choice.equals("Square")){
			System.out.println("Enter the side");
			int side=sc.nextInt();
			Square sq1=new Square("Sq1",side);
			System.out.printf("%.2f",sq1.calculateArea());
		}
		sc.close();
	}

}

2. 
package Day4;

public interface AdvancedArithmetic {
	int divisor_sum(int n);

}

package Day4;

import java.util.Scanner;

class MyCalculator implements AdvancedArithmetic {
	public int divisor_sum(int n){
		int sum=0;
		for (int i=1;i<=n;i++){
			if (n%i==0){
				sum=sum+i;
			}
		}
		return sum;
	}
public static void main(String[] args){
	MyCalculator mc=new MyCalculator();
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the number");
	int n=sc.nextInt();
	System.out.println(mc.divisor_sum(n));
	sc.close();
}
}

3. 
package Day4;

import java.util.Scanner;

public class Main1 {
	public static void main(String[] args){
		System.out.println("Select the card\n1.Membership Card\n2.Payback Card");
		Scanner sc=new Scanner(System.in);
		int choice=sc.nextInt();
		if (choice==1){
			sc.nextLine();
			System.out.println("Enter the card details seperated by a |:");
			String info=sc.nextLine();
			System.out.println("Enter the rating");
			Integer rating=Integer.valueOf(sc.nextInt());
			String[] strarr=info.split("\\|");
			MembershipCard mc=new MembershipCard(strarr[0],strarr[1],strarr[2],rating);
			System.out.println(mc.getHolderName()+"'s Membership Card Details");
			System.out.println("Card Number "+mc.getCardNumber());
			System.out.println("Expiry Date "+mc.getExpiryDate());
			System.out.println("Rating "+mc.getRating());
			
		}
		else if(choice==2){
			sc.nextLine();
			System.out.println("Enter the card details seperated by a |:");
			String info=sc.nextLine();
			String[] strarr=info.split("\\|");
			System.out.println("Enter points in card");
			Integer points=Integer.valueOf(sc.nextInt());
			System.out.println("Enter the total amount");
			Double totalAmount=Double.valueOf(sc.nextDouble());
	        PaybackCard pc=new PaybackCard(strarr[0], strarr[1], strarr[2], points, totalAmount);
	        System.out.println(pc.getHolderName()+"'s Payback Card Details:\n"+"Card Number "+pc.getCardNumber());
	        System.out.println("Expiry Date "+pc.getExpiryDate());
	        System.out.println("Points Earned "+pc.getPointsEarned());
	        System.out.println("Total Amount "+pc.getTotalAmount());
		}
		sc.close();
	}

}
package Day4;

public class MembershipCard extends Card{
	private Integer rating;

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public MembershipCard(String holderName, String cardNumber, String expiryDate, Integer rating) {
		super(holderName, cardNumber, expiryDate);
		this.rating = rating;
	}
	public MembershipCard(){
		
	}
	

}
package Day4;

public class PaybackCard extends Card{
	private Integer pointsEarned;
	private Double totalAmount;
	public Integer getPointsEarned() {
		return pointsEarned;
	}
	public void setPointsEarned(Integer pointsEarned) {
		this.pointsEarned = pointsEarned;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public PaybackCard(){
		
	}
	public PaybackCard(String holderName, String cardNumber, String expiryDate, Integer pointsEarned,Double totalAmount) {
		super(holderName, cardNumber, expiryDate);
		this.pointsEarned = pointsEarned;
		this.totalAmount = totalAmount;
	}
	

}
package Day4;

public abstract class Card {
	protected String holderName;
	protected String cardNumber;
	protected String expiryDate;
	public String getHolderName() {
		return holderName;
	}
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Card(){
		
	}
	public Card(String holderName, String cardNumber, String expiryDate) {
		super();
		this.holderName = holderName;
		this.cardNumber = cardNumber;
		this.expiryDate = expiryDate;
	}
	

}

4. 
The variable ‘a’ in class A is private. It cannot be directly accessed or set to a value. That’s we get an error. We need to the getter and setter for such cases.
System.out.println("objA.a = "+objA.getA()); 
		objA.setA(222);

5.
in the constructor of class FirstClass: 
a = 100
a = 333
---in the constructor of class B: 
b = 123.45
b = 3.14159
in main(): 
objA.a = 333
objB.b = 3.14159
objA.a = 222
objB.b = 333.33

6. 
class A {
int a = 100;
public void display() {
System.out.printf("a in A = %d\n", a);
}
}
class B extends A{
int b=200;
public void display(){
System.out.printf("b in B = %d\n",b);
}
}
class C extends B{
int c=300;
public void display(){
System.out.printf("c in C =%d\n",c);
}}
public class OOPExercises{
static int a = 555;
public static void main(String[] args) { 
A objA = new A(); 
B objB1 = new B(); 
A objB2 = new B();
C objC1 = new C(); 
B objC2 = new C(); 
A objC3 = new C(); 
objA.display(); 
objB1.display();
objB2.display(); 
objC1.display(); 
objC2.display(); 
objC3.display(); } }


Output:
a in A = 100
b in B = 200
b in B = 200
c in C =300
c in C =300
c in C =300
