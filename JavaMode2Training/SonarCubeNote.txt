Steps for download the sonarQube:
-------------------------------------
1) https://www.sonarqube.org/downloads/
2)Environment variable path =..\Software\sonarqube-7.0\sonarqube-7.0\bin\windows-x86-64 ;%path%
3) \Software\sonarqube-7.0\sonarqube-7.0\bin\windows-x86-64\StartSonar
4)Open a web browser 
http://ip address:9000
    or 
http://localhost:9000
If not opening disable the proxy in the browser 

 

5)
pom.xml
<properties>
        <java.version>1.8</java.version>
        <sonar.sources>src/main</sonar.sources>
        <sonar.tests>src/test</sonar.tests>
        <!-- Optional URL to server. Default value is http://localhost:9000 -->
        <sonar.host.url>
            http://localhost:9000
        </sonar.host.url>
    </properties>

 

    <build>
        <plugins>
            <plugin>
                <groupId>org.sonarsource.scanner.maven</groupId>
                <artifactId>sonar-maven-plugin</artifactId>
                <version>3.2</version>
            </plugin>
        </plugins>
    </build>
6) cmd > mvn clean install
7) cmd > mvn sonar:sonar