package com.springbooteurekaclient1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;



@SpringBootApplication
/*@EnableEurekaClient*/
public class Springbooteurekaclient1Application {

	public static void main(String[] args) {
		SpringApplication.run(Springbooteurekaclient1Application.class, args);
	}
	@Bean 
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
		
	}
	

}
