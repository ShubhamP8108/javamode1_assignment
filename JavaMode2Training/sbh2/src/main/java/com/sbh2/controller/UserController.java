package com.sbh2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sbh2.model.User;
import com.sbh2.service.UserService;

@RestController
public class UserController {
	@Autowired
	private UserService userService;
	
	@GetMapping(value="/")
	public List<User> allUsers(){
		return userService.getUsers();
	}
	@PostMapping(value="create")
	public User saveUser(@RequestBody User user) {
		return userService.saveAndUpdate(user);
	}
	@DeleteMapping("/delete/{param}")
	public void deleteUser(@PathVariable("param") Integer id) {
		userService.deleteByUserId(id);
	}

}
