package com.sbh2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.sbh2.model.User;

public interface UserRepository extends CrudRepository<User,Integer> {
	
	@Query("from user")
	List<User> findAllUser();

}
