package com.sbh2.service;

import java.util.List;

import com.sbh2.model.User;

public interface UserService {

	public abstract List<User>getUsers();
	public abstract User saveAndUpdate(User user);
	public abstract void deleteByUserId(Integer id);
}
