package com.sbh2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbh2.model.User;
import com.sbh2.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public List<User> getUsers() {
		return userRepository.findAllUser();
		
	}

	@Override
	public User saveAndUpdate(User user) {
		return userRepository.save(user);
	}

	@Override
	public void deleteByUserId(Integer id) {
    userRepository.deleteById(id);
	}

}
