package com.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.exception.UserException;
import com.model.User;
@Repository
public class UserDaoImpl implements UserDao {
	@Autowired
	private SessionFactory sessionFactory;
    @Autowired
    private UserException userException;
	@Override
	public User getUser() {
		Session session=sessionFactory.openSession();
		String hqlread="from User";
		List<User> users=session.createQuery(hqlread).list();
		return users.get(0);
	}

	@Override
	public User getUserByName(String userName) {
		Session session=sessionFactory.openSession();
		String hqlread="from User as u where u.userName= :alias";
		Query query=session.createQuery(hqlread);
		query.setParameter("alias", userName);
		List<User> list=query.getResultList();
		return list.get(0);
	}

	@Override
	public User getUserById(Integer userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> getUsers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User addUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User updateUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User deleteUser(Integer userId) {
		// TODO Auto-generated method stub
		return null;
	}

}
