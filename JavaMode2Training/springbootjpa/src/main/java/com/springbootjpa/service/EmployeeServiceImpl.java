package com.springbootjpa.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springbootjpa.dao.EmployeeRepository;
import com.springbootjpa.model.Employee;
@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
	private EmployeeRepository employeeRepository;  //wire==DI(@Autowired)
	
	@Override
	//@Transactional    no need for listing
	public List<Employee> listAll() {
		return employeeRepository.findAll();  //select * from employee table
	}

	@Override
	//@Transactional
	public Employee readByEmployeeId(Integer abc) {
	Employee employee=null;  //we creating an object for the if method
	Optional<Employee> optional	= employeeRepository.findById(abc);
	
	if(optional.isPresent()) {
		employee=optional.get();     //it is in flower bracket so it becomes local variable so we create an external object
	}
	else {
		System.out.println("no such employee");  //or else we have to raise an exception
	
	}
		return employee;
	}

	@Override
	@Transactional
	public Employee save(Employee employee) {    //ORM
		return  employeeRepository.save(employee);
		
	}

	@Override
	@Transactional
	public Integer delete(Integer efg) {
		employeeRepository.deleteById(efg);  //because it gives void as return type so returning efg
		return efg;
	}

	@Override
	public List<String> searchByDesignation(String hij) {
		return employeeRepository.getByDesignation(hij);
	}

	@Override
	public Optional<Employee> searchByIdAndDesignation(Integer lmn, String hij) {
		return employeeRepository.findByEmpIdAndDesignation(lmn, hij);
	}

	@Override
	public Employee nativeQuery(Integer lmn, String hij) {
		return employeeRepository.findByNativeQuery(lmn, hij);
	}

	@Override
	public Employee buitIn(Integer tea, String coffee) {
		return employeeRepository.findByEmpIdAndEmpName(tea, coffee);
	}
	

}
