package com.springbootjpa.service;

import java.util.List;
import java.util.Optional;

import com.springbootjpa.model.Employee;

//perform the crud operation here
public interface EmployeeService {

    public abstract List<Employee>listAll();     //read
    public abstract Employee readByEmployeeId(Integer abc);   //for reading by id
    public abstract Employee save(Employee employee);   //create
    public abstract Integer delete(Integer efg);        //delete
    public abstract List<String> searchByDesignation(String hij);
    public abstract Optional<Employee> searchByIdAndDesignation(Integer lmn ,String hij);
    public abstract Employee nativeQuery(Integer lmn,String hij);
    public abstract Employee buitIn(Integer tea,String coffee);
}
