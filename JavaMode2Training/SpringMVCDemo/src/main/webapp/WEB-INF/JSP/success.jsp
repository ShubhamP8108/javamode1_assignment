<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
.tablepro
{border-collapse:collapse;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table border="1" class="tablepro">
<tr><th>Id</th><th>Name</th><th>languages</th><th>Country</th></tr>
<c:forEach var="user" items="${listOfUsers}">
<tr>
<td>${user.userId}</td>
<td>${user.username}</td>
<td>${user.language}</td>
<td>${user.country}</td>
</tr>
</c:forEach>
</table>
</body>
</html>