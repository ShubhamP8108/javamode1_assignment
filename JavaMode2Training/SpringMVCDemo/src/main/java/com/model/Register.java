package com.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
@Entity
@Table(name="register_details")
public class Register {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int userid;
	
	@NotEmpty(message="Username should not be empty")
private String username;
	@NotEmpty(message="password should not be empty")
private String password;
	@NotEmpty(message="language should be choosen")
private String language;
	@NotEmpty(message="gender should be selected")
private String gender;
	@NotEmpty(message="select country")
	private String country;
	@NotNull(message="select date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	//private LocalDate date;
	//@DateTimeFormat(pattern = "yyyy-MM-dd")
  //@NotNull
       //@Temporal(TemporalType.DATE)
    private Date date;
public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getLanguage() {
	return language;
}
public void setLanguage(String language) {
	this.language = language;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
}
