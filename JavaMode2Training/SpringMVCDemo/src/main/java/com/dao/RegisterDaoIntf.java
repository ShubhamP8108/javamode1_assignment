package com.dao;

import com.model.Register;

public interface RegisterDaoIntf {

	void saveRegister(Register register);

}
