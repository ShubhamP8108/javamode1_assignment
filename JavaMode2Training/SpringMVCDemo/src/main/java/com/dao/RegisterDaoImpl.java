package com.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.model.Register;

@Repository
public class RegisterDaoImpl implements RegisterDaoIntf {
/*@Autowired
	JdbcTemplate template;
	public void saveRegister(Register register) {
		java.sql.Date sqlDate=new java.sql.Date(register.getDate().getTime());
		
		template.update("insert into registerinspring values('"+register.getUsername()+"','"+register.getPassword()+"','"+register.getLanguage()+"','"+register.getGender()+"','"+register.getCountry()+"','"+sqlDate+"')");
		
				
	}*/
	
	@Autowired
	SessionFactory sessionFactory;
   public void saveRegister(Register register) {
	   sessionFactory.openSession().save(register);
   }
   public List<Register> getUsers()
   {
   List<Register> ls=sessionFactory.openSession().createQuery("from Register").list();
   return ls;
   }
}
