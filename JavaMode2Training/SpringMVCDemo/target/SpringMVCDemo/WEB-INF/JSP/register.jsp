<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<!-- <style>
.error
{
color:red;
}
</style> -->
</head>
<body>
<form:form action="saveRegister" modelAttribute="register">
Username:<form:input path="username"/><form:errors path="username"/><br>
Password:<form:password path="password"/><form:errors path="password"/><br>
Languages:
C<form:checkbox path="language" value="C"/>
Java<form:checkbox path="language" value="Java"/>
Python<form:checkbox path="language" value="Python"/><form:errors path="language"/><br>
Gender:
Male:<form:radiobutton path="gender" value="Male"/>
Female:<form:radiobutton path="gender" value="Female"/><form:errors path="gender"/><br>
country:<form:select path="country"><form:errors path="country"/><br>
<form:option value=''>select</form:option>
<form:option value='India'/>
<form:option value='US'/><br>

</form:select>
Date:<form:input type="date" path="date"/><form:errors path="date"/><br>
<input type="submit" value="submit"/>

</form:form>

</body>
</html>