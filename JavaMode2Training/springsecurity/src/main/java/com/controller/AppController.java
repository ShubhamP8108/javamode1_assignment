package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AppController {

 

    @RequestMapping(value = "/")
    public ModelAndView visitHome() {
        return new ModelAndView("index");
    }

 

    @RequestMapping(value = "/admin")
    public ModelAndView visitAdmin() {
        ModelAndView model = new ModelAndView("admin");
        model.addObject("title", "Admministrator Control Panel");
        model.addObject("message", "This page demonstrates how to use Spring security.");

 

        return model;
    }
    
    @RequestMapping(value = "/raina")
    public ModelAndView normal() {
        ModelAndView model = new ModelAndView("hello");
        model.addObject("title", "User page");
        model.addObject("message", "This user page demonstrates how to use Spring security.");

 

        return model;
    }
}





