package com.exception;

import java.io.IOException;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalException {
	@ExceptionHandler({IOException.class,UserException.class})//instead of writing many @exception handlers we can write in one line itself
	public ModelAndView processException(Exception e) {
		ModelAndView modelAndView=new  ModelAndView("exceptionPage");
		modelAndView.addObject("reason","Reason for"+ e.getMessage());
		return modelAndView;

	}
}
