package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.User;

public class Main {

	public static void main(String[] args) {
   
    ApplicationContext applicationcontext=new ClassPathXmlApplicationContext("com/config/springcontext.xml");
	User obj=(User)applicationcontext.getBean("user");
	obj.setUserID(123);
	obj.setUserName("Hello");
	obj.setPassword("sec");
	System.out.println("Display");
	System.out.println("User id:"+obj.getUserID());
	System.out.println("User name:"+obj.getUserName());
	
	}

}
