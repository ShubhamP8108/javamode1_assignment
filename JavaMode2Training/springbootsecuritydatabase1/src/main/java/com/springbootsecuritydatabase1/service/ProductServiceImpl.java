package com.springbootsecuritydatabase1.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springbootsecuritydatabase1.model.Product;
import com.springbootsecuritydatabase1.repository.ProductRepository;
@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductRepository productRepository;

	@Override
	public List<Product> listAll() {
		return productRepository.findAll();
	}

	@Override
	public Product save(Product product) {
		return productRepository.save(product);
	}

	@Override
	public Product get(Long id) {
		return productRepository.getById(id);
	}

	@Override
	public Long delete(Long id) {
		productRepository.deleteById(id);
		return id;
	}

}
