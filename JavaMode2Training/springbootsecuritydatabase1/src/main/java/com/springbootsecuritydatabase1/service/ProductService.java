package com.springbootsecuritydatabase1.service;

import java.util.List;

import com.springbootsecuritydatabase1.model.Product;

public interface ProductService {
    public abstract List<Product> listAll();
    public abstract  Product save(Product product);
    public abstract  Product get(Long id) ;
    public abstract  Long delete(Long id);
}