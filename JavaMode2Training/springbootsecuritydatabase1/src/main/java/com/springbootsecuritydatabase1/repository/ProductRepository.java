package com.springbootsecuritydatabase1.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springbootsecuritydatabase1.model.Product;

public interface ProductRepository extends JpaRepository<Product,Long>{

}
