package com.springbootsecuritydatabase1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootsecuritydatabase1Application {

	public static void main(String[] args) {
		SpringApplication.run(Springbootsecuritydatabase1Application.class, args);
	}

}
