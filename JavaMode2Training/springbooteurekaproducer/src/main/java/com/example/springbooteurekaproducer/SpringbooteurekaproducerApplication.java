package com.example.springbooteurekaproducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SpringbooteurekaproducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbooteurekaproducerApplication.class, args);
	}

}
