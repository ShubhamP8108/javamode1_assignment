package com.example.springbooteurekaproducer.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.springbooteurekaproducer.model.Employee;

@RestController
public class EmployeeController {
@GetMapping(value="search/{abc}")
public ResponseEntity<Employee> getEmployeeBYId(@PathVariable("abc") Integer empNo){
	Employee employee=null;
	if(empNo==10) {
		employee=new Employee(10,"ten",1010.10f);
		
	}
	if(empNo==20) {
		employee=new Employee(20,"twenty",2010.10f);
		
	}
	return new ResponseEntity<Employee>(employee,HttpStatus.OK);
}
}
