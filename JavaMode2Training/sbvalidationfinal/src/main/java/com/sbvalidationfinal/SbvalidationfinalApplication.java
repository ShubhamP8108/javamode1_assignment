package com.sbvalidationfinal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbvalidationfinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbvalidationfinalApplication.class, args);
	}

}
