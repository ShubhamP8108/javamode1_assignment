package com.springbootdemo1.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.springbootdemo1.model.Student;
@Service
public class StudentServiceImpl implements StudentServiceIntf {

	@Override
	public Student saveStudent(Student student) {
		System.out.println("Student :" + student.getStudentId());
		System.out.println("Student :" + student.getStudentName());
		return student;
	}

	@Override
	public Student readStudentById(Integer studentId) {
		Student student=null;
		if(studentId==10) {
			student =new Student();
			student.setStudentId(studentId);
			student.setStudentName("jain");
			student.setMark1(80);
			student.setMark2(90);
		}
		return student;
	}

	@Override
	public Student updateStudent(Student student) {
		System.out.println("student update:"+student.getStudentId());
		System.out.println("student update:"+student.getStudentName());

		return student;
	}

	@Override
	public boolean deleteStudent(Integer studentId) {
		return false;
	}

	@Override
	public List<Student> readAll() {
		List<Student> students=new ArrayList();
		Student student=new Student();
		student.setStudentId(25);
		student.setStudentName("mango");
		student.setMark1(97);
		student.setMark2(80);
		Student student1=new Student();
        student1.setStudentId(28);
        student1.setStudentName("banana");
        student1.setMark1(95);
        student1.setMark2(82);
		students.add(student);
		students.add(student1);

		return students;
	}

}
