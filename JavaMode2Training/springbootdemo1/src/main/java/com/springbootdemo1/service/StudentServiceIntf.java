package com.springbootdemo1.service;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.springbootdemo1.model.Student;

public interface StudentServiceIntf {
	public abstract Student saveStudent( Student student);
    public abstract Student readStudentById( Integer studentId);
    public abstract Student updateStudent( Student student);
    public abstract boolean deleteStudent( Integer studentId);
    public abstract List<Student> readAll();
}
