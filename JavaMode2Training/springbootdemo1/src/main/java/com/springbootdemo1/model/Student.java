package com.springbootdemo1.model;

public class Student {
	
 private Integer studentId;
 private String studentName;
 private int mark1;
 private int mark2;
public Integer getStudentId() {
	return studentId;
}
public void setStudentId(Integer studentId) {
	this.studentId = studentId;
}
public String getStudentName() {
	return studentName;
}
public void setStudentName(String studentName) {
	this.studentName = studentName;
}
public Student() {
	super();
}
public Student(Integer studentId, String studentName,int mark1,int mark2) {
	super();
	this.studentId = studentId;
	this.studentName = studentName;
	this.mark1=mark1;
	this.mark2=mark2;
	
}
public int getMark1() {
	return mark1;
}
public void setMark1(int mark1) {
	this.mark1 = mark1;
}
public int getMark2() {
	return mark2;
}
public void setMark2(int mark2) {
	this.mark2 = mark2;
}

 
}
