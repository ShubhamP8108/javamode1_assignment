package com.springbootvalidation.controller;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.springbootvalidation.model.Employee;

@RestController
@Validated
public class EmployeeController {
	@PostMapping(value="create")
	public ResponseEntity<Employee> createEmployee(@Valid @RequestBody Employee employee){
		System.out.println("server side emp no:"+employee.getEmpNo());
		System.out.println("server side emp name:"+employee.getFirstName());
		System.out.println("server side emp lastnaem:"+employee.getLastName());
        ResponseEntity<Employee> responseEntity=new ResponseEntity<Employee>(employee,HttpStatus.OK);
		return responseEntity;
		
	}
	
	@GetMapping(value="read")
	public Employee readEmployee(){
        return  new Employee(10,"hello","world");
		
	}
	@GetMapping("/search/{id}")
	ResponseEntity<String> readEmployeeById(@PathVariable ("id") @Size(min=5) String id){
		System.out.println("entered id"+id);
		return ResponseEntity.ok("valid");
	}
    
}
