package com.springbootwithspringmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootwithspringmvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootwithspringmvcApplication.class, args);
	}

}
