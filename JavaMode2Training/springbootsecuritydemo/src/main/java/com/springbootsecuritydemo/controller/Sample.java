package com.springbootsecuritydemo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Sample {
	@GetMapping(value="/break")
	public ResponseEntity<String> hello(){
		return new ResponseEntity<String>("break",HttpStatus.OK);
		 
		
	}

}
